// Comprueba en cada uno de los usuarios que tenga al menos dos trimestres aprobados y añade la propiedad isApproved a true o false en consecuencia. Una vez lo tengas compruébalo con un console.log.

const alumns = [
    {name: 'Pepe Viruela', T1: false, T2: false, T3: true}, 
		{name: 'Lucia Aranda', T1: true, T2: false, T3: true},
		{name: 'Juan Miranda', T1: false, T2: true, T3: true},
		{name: 'Alfredo Blanco', T1: false, T2: false, T3: false},
		{name: 'Raquel Benito', T1: true, T2: true, T3: true}
]

for (const alumn of alumns) {
    let counter = 0; 
    for (const value in alumn) {
        if ((value === 'T1' || value === 'T2' || value === 'T3') && alumn[value] === true) {
            counter++;
        }
        alumn.isApproved = (counter >= 2) ? true : false;
    }
}

console.log(alumns);
